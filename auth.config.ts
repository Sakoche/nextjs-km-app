import type { NextAuthConfig } from 'next-auth';
import { redirect } from 'next/navigation';
 
export const authConfig = {
  pages: {
    signIn: '/aba/login',
    newUser: '/auth/new-user',
  },callbacks: {
    authorized({ auth, request: { nextUrl } }) {
      const isLoggedIn = !!auth?.user;
      const isOnSecuredRoute = nextUrl.pathname.startsWith('/home/dash');
     

      if (isOnSecuredRoute) {
        if (isLoggedIn) return true;
        return false; // Redirect unauthenticated users to login page
      } else if (isLoggedIn) {

        return true;
      }

      return true;
    },
  },
  providers: [], // Add providers with an empty array for now
} satisfies NextAuthConfig;