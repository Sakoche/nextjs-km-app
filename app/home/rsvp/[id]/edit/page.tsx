import EditGuestForm from '@/app/ui/wedding/guests/edit-form';
import Breadcrumbs from '@/app/ui/wedding/guests/breadcrumbs';
import { fetchAllFamilies, fetchCustomers, fetchGuestById, fetchInvoiceById } from '@/app/lib/data';
import { cormorant_Garamond, mrs_Saint_Delafield } from '@/app/ui/fonts';

export default async function Page({ params }: { params: { id: string } }) {
  const id = params.id;
  const [guest, families] = await Promise.all([
    fetchGuestById(id),
    fetchAllFamilies(),
  ])
  return (
    <main >
      <Breadcrumbs

        breadcrumbs={[
          {
            label: 'Modifiez votre réponse',
            href: `/dashboard/invoices/${id}/edit`,
            active: true,
          },
        ]}
      />
      {/* <EditGuestForm guest={guest} families={families} /> */}
    </main>
  );
}