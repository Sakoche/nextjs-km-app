import Breadcrumbs from '@/app/ui/invoices/breadcrumbs';
import { fetchAllFamilies } from '@/app/lib/data';
import Form from '@/app/ui/wedding/create-response';
 
export default async function Page({ params }: { params: { id: string } }) {
  const id = params.id;
  const families = await fetchAllFamilies();
 
  return (
    <main>
      <Breadcrumbs
        breadcrumbs={[

          {
            label: '',
            href: '/wedding/create-response',
            active: true,
          },
        ]}
      />
      <Form families={families} />
    </main>
  );
}