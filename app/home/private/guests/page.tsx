import Pagination from '@/app/ui/invoices/pagination';
import Search from '@/app/ui/search';
import Table from '@/app/ui/wedding/guests/table';
import { CreateGuest } from '@/app/ui/wedding/guests/buttons';
import { lusitana } from '@/app/ui/fonts';
import { InvoicesTableSkeleton } from '@/app/ui/skeletons';
import { Suspense } from 'react';
import { fetchGuestsAdulteAndChildren, fetchGuestsByStatus, fetchGuestsPages, fetchInvoicesPages } from '@/app/lib/data';
import { cormorant_Garamond, mrs_Saint_Delafield, dancing_Script, great_Vibes } from '@/app/ui/fonts';

 
export default async function Page({
  searchParams,
}: {
  searchParams?: {
    query?: string;
    page?: string;
  };
}) {

  const query = searchParams?.query || '';
  const currentPage = Number(searchParams?.page) || 1;  
  const totalPages = await fetchGuestsPages(query);
  const totalAcceptedInvitations = await fetchGuestsByStatus('oui');
  const totalRefusedInvitations = await fetchGuestsByStatus('non');
  const {total_children, total_adults} = await fetchGuestsAdulteAndChildren();

  return (
    <div className="w-full">
      <div className="flex w-full items-center justify-between">
        <h1 className={`${lusitana.className} text-2xl`}>Liste des invités</h1>
      </div>
      <div className="mt-4 flex items-center justify-between gap-2 md:mt-8">
        <Search placeholder="Recherchez un invité..." />
        <CreateGuest />
      </div>
       <Suspense key={query + currentPage} fallback={<InvoicesTableSkeleton />}>
        <div className='flex mt-5 gap-5 text-right rounded-lg bg-gray-50 p-10  text-lg text-zinc-800 font-bold'>
          <div className='flex flex-col'>
            <p><span className={`${dancing_Script.className}`}>Invitations acceptée(s) : </span> {totalAcceptedInvitations} </p>
            <p><span className={`${dancing_Script.className}`}>Invitations Refusée(s) :  </span> {totalRefusedInvitations}  </p>
          </div>
        
          <div className='flex flex-col'>
            <p><span className={`${dancing_Script.className}`}>Nombre d&#39; adulte(s) : </span>{total_adults}  </p>
              <p><span className={`${dancing_Script.className}`}>Nombre d&#39; enfants(s) : </span>{total_children}  </p>

         </div>
        


        </div>
        <Table query={query} currentPage={currentPage} />
      </Suspense>
      <div className="mt-5 flex w-full justify-center">
        <Pagination totalPages={totalPages} />
      </div>
    </div>
  );
}