import Breadcrumbs from '@/app/ui/invoices/breadcrumbs';
import { fetchAllFamilies, fetchCustomers } from '@/app/lib/data';
import Form from '@/app/ui/wedding/familles/create-form';
 
export default async function Page({ params }: { params: { id: string } }) {
  const id = params.id;
  const families = await fetchAllFamilies();
 

  return (
    <main>
      <Breadcrumbs
        breadcrumbs={[
          { label: 'Families', href: '/home/families/' },
          {
            label: 'Créer une famille',
            href: '/home/families/create',
            active: true,
          },
        ]}
      />
      <Form families={families} />
    </main>
  );
}