import EditGuestForm from '@/app/ui/wedding/guests/edit-form';
import Breadcrumbs from '@/app/ui/wedding/guests/breadcrumbs';
import { fetchAllFamilies, fetchCustomers, fetchFamillyById, fetchGuestById, fetchInvoiceById } from '@/app/lib/data';
import { cormorant_Garamond, mrs_Saint_Delafield } from '@/app/ui/fonts';
import EditFamilyForm from '@/app/ui/wedding/familles/edit-form';

export default async function Page({ params }: { params: { id: string } }) {
  const id = params.id;
  const [familly] = await Promise.all([
    fetchFamillyById(id),
  ])
  return (
    <main >
      <Breadcrumbs

        breadcrumbs={[
          {
            label: 'Modifiez votre réponse',
            href: `/home/private/families/${id}/edit`,
            active: true,
          },
        ]}
      />
      { <EditFamilyForm familly={familly} /> }
    </main>
  );
}