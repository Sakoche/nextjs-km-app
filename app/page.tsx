
import { cormorant_Garamond, mrs_Saint_Delafield } from '@/app/ui/fonts';
import { InvoicesTableSkeleton } from '@/app/ui/skeletons';
import { Suspense } from 'react';
import styles from './page.module.css';
import Image from 'next/image'
import Link from 'next/link';
import { InvitationCard } from './ui/wedding/invitationCard/invitation-card';
import { Rsvp } from './ui/wedding/rsvp/rsvp';
export default async function Page({
  searchParams,
}: {
  searchParams?: {
    query?: string;
    page?: string;
  };
}) {

  const query = searchParams?.query || '';
  // const currentPage = Number(searchParams?.page) || 1;  
  // const totalPages = await fetchInvoicesPages(query);
  
  return (
    <div className="min-h-screen flex flex-1 items-center justify-center p-9">
      <InvitationCard />
   
     
    </div>
  );
}