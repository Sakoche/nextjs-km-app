import Breadcrumbs from '@/app/ui/invoices/breadcrumbs';
import { fetchAllFamilies } from '@/app/lib/data';
import { Contact } from '../ui/wedding/contact/contact';
 
export default async function Page({ params }: { params: { id: string } }) {
  const id = params.id;
  const families = await fetchAllFamilies();
 
  return (
    <main>
      <Breadcrumbs
        breadcrumbs={[

          {
            label: '',
            href: '/contact',
            active: true,
          },
        ]}
      />
      <Contact />
    </main>
  );
}