
import { cormorant_Garamond, mrs_Saint_Delafield} from '@/app/ui/fonts';
import styles from './thankyouPage.module.css';

import clsx from 'clsx';
import Link from 'next/link';
import { ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
export default async function Page({
  searchParams,
}: {
  searchParams?: {
    query?: string;
    page?: string;
  };
}) {

  const query = searchParams?.query || '';
  // const currentPage = Number(searchParams?.page) || 1;  
  // const totalPages = await fetchInvoicesPages(query);
  
  return (
    <div className="  flex flex-1  items-center justify-center">
      <div className={clsx( styles.container,` flex flex-1 max-w-screen-lg  shadow-md rounded-lg items-center pt-36 pb-36 justify-center flex-col w-2/5 relative h-[717px] bg-white   text-zinc-400 ${cormorant_Garamond.className}`)}>
        <div className={`flex items-center w-5/5 flex-col flex-1 bg-[url('/wedding/home_images/bg.png')] bg-contain bg-no-repeat bg-center  ${styles.card} ${styles.bg}  `}>
          <div className={` ${styles.appear1} pt-30  flex flex-col justify-center align-center  text-zinc-500 ${styles.invitSentence} relative  ${cormorant_Garamond.className}`}>
            <p>Merci, votre réponse à bien été prise en compte.</p>
          </div>
        </div>
        <Link href="/home/rsvp" className='absolute bottom-20 cursor-pointer'>
          <ArrowLeftCircleIcon className="w-6 text-zinc-500" />
        </Link>
      </div>
    
    </div>
  );
}