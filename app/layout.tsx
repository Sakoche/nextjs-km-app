import '@/app/ui/global.css';
import { inter } from '@/app/ui/fonts';
import NextAuthProvider from './NextAuthProvider';

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`${inter.className} antialiased`}>
      <NextAuthProvider>
        {children}

      </NextAuthProvider>
        </body>
    </html>
  );
}
