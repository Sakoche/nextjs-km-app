import { sql } from '@vercel/postgres';
import {
  CustomerField,
  CustomersTable,
  InvoiceForm,
  InvoicesTable,
  LatestInvoiceRaw,
  User,
  Revenue,
  GuestField,
  GuestsTable,
  GuestForm,
  FamilyForm,
} from './definitions';
import { formatCurrency } from './utils';
import { unstable_noStore as noStore } from 'next/cache';


export async function fetchRevenue() {
  noStore();
  // Add noStore() here prevent the response from being cached.
  // This is equivalent to in fetch(..., {cache: 'no-store'}).

  try {
    // Artificially delay a reponse for demo purposes.
    // Don't do this in real life :)

    console.log('Fetching revenue data...');
    await new Promise((resolve) => setTimeout(resolve, 3000));

    const data = await sql<Revenue>`SELECT * FROM revenue`;

    console.log('Data fetch complete after 3 seconds.');

    return data.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch revenue data.');
  }
}

export async function fetchLatestInvoices() {
  noStore();

  try {
    const data = await sql<LatestInvoiceRaw>`
      SELECT invoices.amount, customers.name, customers.image_url, customers.email, invoices.id
      FROM invoices
      JOIN customers ON invoices.customer_id = customers.id
      ORDER BY invoices.date DESC
      LIMIT 5`;

    const latestInvoices = data.rows.map((invoice) => ({
      ...invoice,
      amount: formatCurrency(invoice.amount),
    }));
    return latestInvoices;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch the latest invoices.');
  }
}

export async function fetchAllFamilies() {
  noStore();

  try {
    const data = await sql<any>`
      SELECT *
      FROM families
      ORDER BY name DESC`;
  
    return data.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch all families.');
  }
}

export async function fetchCardData() {
  noStore();

  try {
    // You can probably combine these into a single SQL query
    // However, we are intentionally splitting them to demonstrate
    // how to initialize multiple queries in parallel with JS.
    const invoiceCountPromise = sql`SELECT COUNT(*) FROM invoices`;
    const customerCountPromise = sql`SELECT COUNT(*) FROM customers`;
    const invoiceStatusPromise = sql`SELECT
         SUM(CASE WHEN status = 'paid' THEN amount ELSE 0 END) AS "paid",
         SUM(CASE WHEN status = 'pending' THEN amount ELSE 0 END) AS "pending"
         FROM invoices`;

    const data = await Promise.all([
      invoiceCountPromise,
      customerCountPromise,
      invoiceStatusPromise,
    ]);

    const numberOfInvoices = Number(data[0].rows[0].count ?? '0');
    const numberOfCustomers = Number(data[1].rows[0].count ?? '0');
    const totalPaidInvoices = formatCurrency(data[2].rows[0].paid ?? '0');
    const totalPendingInvoices = formatCurrency(data[2].rows[0].pending ?? '0');

    return {
      numberOfCustomers,
      numberOfInvoices,
      totalPaidInvoices,
      totalPendingInvoices,
    };
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to card data.');
  }
}

const ITEMS_PER_PAGE = 6;
export async function fetchFilteredInvoices(
  query: string,
  currentPage: number,
) {
  noStore();

  const offset = (currentPage - 1) * ITEMS_PER_PAGE;

  try {
    const invoices = await sql<InvoicesTable>`
      SELECT
        invoices.id,
        invoices.amount,
        invoices.date,
        invoices.status,
        customers.name,
        customers.email,
        customers.image_url
      FROM invoices
      JOIN customers ON invoices.customer_id = customers.id
      WHERE
        customers.name ILIKE ${`%${query}%`} OR
        customers.email ILIKE ${`%${query}%`} OR
        invoices.amount::text ILIKE ${`%${query}%`} OR
        invoices.date::text ILIKE ${`%${query}%`} OR
        invoices.status ILIKE ${`%${query}%`}
      ORDER BY invoices.date DESC
      LIMIT ${ITEMS_PER_PAGE} OFFSET ${offset}
    `;

    return invoices.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch invoices.');
  }
}
export async function fetchFilteredGuests(
  query: string,
  currentPage: number,
) {
  noStore();

  const offset = (currentPage - 1) * ITEMS_PER_PAGE;

  const invoiceStatusPromise = sql`SELECT
  SUM(CASE WHEN status = 'paid' THEN amount ELSE 0 END) AS "paid",
  SUM(CASE WHEN status = 'pending' THEN amount ELSE 0 END) AS "pending"
  FROM invoices`;

  try {
    const guests = await sql<GuestsTable>`
      SELECT
        * 
      FROM guests
      WHERE
      guests.adult_one_firstname ILIKE ${`%${query}%`} OR
      guests.adult_one_lastname ILIKE ${`%${query}%`} 

      ORDER BY guests.adult_one_lastname ASC
      LIMIT ${ITEMS_PER_PAGE} OFFSET ${offset}
    `;

    return guests.rows;
  } catch (error) {
    console.error('Database Error filtered:', error);
    throw new Error('Failed to fetch guests.');
  }
}
export async function fetchFilteredFamilies(
  query: string,
  currentPage: number,
) {
  noStore();

  const offset = (currentPage - 1) * ITEMS_PER_PAGE;

  try {
    const invoices = await sql<GuestsTable>`
      SELECT
        *
      FROM families
      WHERE
        families.name ILIKE ${`%${query}%`} OR
        families.email ILIKE ${`%${query}%`} 
      ORDER BY families.name DESC
      LIMIT ${ITEMS_PER_PAGE} OFFSET ${offset}
    `;

    return invoices.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch families.');
  }
}
export async function fetchInvoicesPages(query: string) {
  noStore();

  try {
    const count = await sql`SELECT COUNT(*)
    FROM invoices
    JOIN customers ON invoices.customer_id = customers.id
    WHERE
      customers.name ILIKE ${`%${query}%`} OR
      customers.email ILIKE ${`%${query}%`} OR
      invoices.amount::text ILIKE ${`%${query}%`} OR
      invoices.date::text ILIKE ${`%${query}%`} OR
      invoices.status ILIKE ${`%${query}%`}
  `;

    const totalPages = Math.ceil(Number(count.rows[0].count) / ITEMS_PER_PAGE);
    return totalPages;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch total number of invoices.');
  }
}

export async function fetchGuestsPages(query: string) {
  noStore();

  try {
    const count = await sql`SELECT COUNT(*)
    FROM guests
    WHERE
    adult_one_firstname ILIKE ${`%${query}%`} OR
    adult_one_lastname ILIKE ${`%${query}%`} OR
    adult_two_lastname ILIKE ${`%${query}%`} OR
    adult_two_firstname ILIKE ${`%${query}%`}
    GROUP BY adult_one_lastname
    ORDER BY adult_one_lastname DESC

  `;
   if(!count?.rows[0]) {
      return 0;
   }

    const totalPages = Math.ceil(Number(count.rows[0].count) / ITEMS_PER_PAGE);
    return totalPages;
  } catch (error) {
    console.error('Database Error guests:', error);
    throw new Error('Failed to fetch total number of guests.');
  }
}

export async function fetchGuestsByStatus(status: string) {
  noStore();

  try {
    const count = await sql`SELECT COUNT(*), SUM(total_adults) as total_adults, SUM(total_children) as total_children
    FROM guests
    WHERE
    status LIKE ${`%${status}%`} 
  `;
  console.log('count response', count);

    return count.rows[0].count;
  } catch (error) {
    console.error('Database Error guests:', error);
    throw new Error('Failed to fetch total number of guests.');
  }
}

export async function fetchGuestsAdulteAndChildren() {
  noStore();

  try {
    const response = await sql`SELECT SUM(total_children) as total_children , SUM(total_adults) as total_adults
    FROM guests
  `;

    return response.rows[0];
  } catch (error) {
    console.error('Database Error guests:', error);
    throw new Error('Failed to fetch total number of guests.');
  }
}

export async function fetchFamiliesPages(query: string) {
  noStore();

  try {
    const count = await sql`SELECT COUNT(*)
    FROM families
    WHERE
    families.name ILIKE ${`%${query}%`} OR
    families.email ILIKE ${`%${query}%`} 
  `;

    const totalPages = Math.ceil(Number(count.rows[0].count) / ITEMS_PER_PAGE);
    return totalPages;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch total number of families.');
  }
}

export async function fetchInvoiceById(id: string) {
  noStore();

  try {
    const data = await sql<InvoiceForm>`
      SELECT
        invoices.id,
        invoices.customer_id,
        invoices.amount,
        invoices.status
      FROM invoices
      WHERE invoices.id = ${id};
    `;

    const invoice = data.rows.map((invoice) => ({
      ...invoice,
      // Convert amount from cents to dollars
      amount: invoice.amount / 100,
    }));

    return invoice[0];
  } catch (error) {
    console.error('Database Error:', error);
  }
}

export async function fetchGuestById(id: string) {
  noStore();

  try {
    const data = await sql<GuestForm>`
      SELECT
       *
      FROM guests
    `;

    const guest = data.rows.map((guest) => ({
      ...guest,
      // Convert amount from cents to dollars
   //   amount: invoice.amount / 100,
    }));

    console.log(guest)

    return guest[0];
  } catch (error) {
    console.error('Database Error:', error);
  }
}
export async function fetchFamillyById(id: string) {
  noStore();

  try {
    const data = await sql<FamilyForm>`
      SELECT
       *
      FROM families
      WHERE families.id = ${id};
    `;

    console.log('data fam', data);
    const familly = data.rows[0];
    return familly;
  } catch (error) {
    console.error('Database Error:', error);
  }
}
export async function fetchCustomers() {
  noStore();

  try {
    const data = await sql<CustomerField>`
      SELECT
        id,
        name
      FROM customers
      ORDER BY name ASC
    `;

    const customers = data.rows;
    return customers;
  } catch (err) {
    console.error('Database Error:', err);
    throw new Error('Failed to fetch all customers.');
  }
}

export async function fetchGuests() {
  noStore();

  try {
    const data = await sql<GuestField>`
      SELECT
        *
      FROM guests
      ORDER BY adult_one_lastname DESC

    `;

    const guests = data.rows;
    console.log('guests', guests);

    return guests;
  } catch (err) {
    console.error('Database Error:', err);
    throw new Error('Failed to fetch all guests.');
  }
}

export async function fetchFilteredCustomers(query: string) {
  noStore();

  try {
    const data = await sql<CustomersTable>`
		SELECT
		  customers.id,
		  customers.name,
		  customers.email,
		  customers.image_url,
		  COUNT(invoices.id) AS total_invoices,
		  SUM(CASE WHEN invoices.status = 'pending' THEN invoices.amount ELSE 0 END) AS total_pending,
		  SUM(CASE WHEN invoices.status = 'paid' THEN invoices.amount ELSE 0 END) AS total_paid
		FROM customers
		LEFT JOIN invoices ON customers.id = invoices.customer_id
		WHERE
		  customers.name ILIKE ${`%${query}%`} OR
        customers.email ILIKE ${`%${query}%`}
		GROUP BY customers.id, customers.name, customers.email, customers.image_url
		ORDER BY customers.name ASC
	  `;

    const customers = data.rows.map((customer) => ({
      ...customer,
      total_pending: formatCurrency(customer.total_pending),
      total_paid: formatCurrency(customer.total_paid),
    }));

    return customers;
  } catch (err) {
    console.error('Database Error:', err);
    throw new Error('Failed to fetch customer table.');
  }
}

export async function getUser(email: string) {
  noStore();

  try {
    const user = await sql`SELECT * from USERS where email=${email}`;
    return user.rows[0] as User;
  } catch (error) {
    console.error('Failed to fetch user:', error);
    throw new Error('Failed to fetch user.');
  }
}
