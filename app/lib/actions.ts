'use server';
 
import { z } from 'zod';
import { sql } from '@vercel/postgres';
import { revalidatePath } from 'next/cache';
import { redirect } from 'next/navigation';
import { unstable_noStore as noStore } from 'next/cache';
import { signIn } from '@/auth';
import { AuthError } from 'next-auth';

const FormSchema = z.object({
    id: z.string(),
    customerId: z.string(),
    amount: z.coerce.number(),
    status: z.enum(['pending', 'paid']),
    date: z.string(),
  });

  const FormResponseSchema = z.object({
    status:  z.enum(['pending', 'oui', 'non']),
    adultOneFirstname:  z.string(),
    adultOneLastname:  z.string(),
    adultTwoFirstname:  z.string(),
    adultTwoLastname:  z.string(),

    childOneFirstname:  z.string(),
    childOneLastname:  z.string(),
    childTwoFirstname:  z.string(),
    childTwoLastname:  z.string(),

    updatedAt: z.string(),
  });
  const CreateFamilyFormSchema = z.object({
    name: z.string(),
    email: z.string(),
    updatedAt: z.string(),
    members: z.coerce.number(),
  });
  const UpdateFamilyFormSchema = z.object({
    id: z.string(),
    name: z.string(),
    email: z.string(),
    members: z.coerce.number(),
    updatedAt: z.string(),

  });
  
   
const CreateInvoice = FormSchema.omit({ id: true, date: true }); 
const CreateResponse = FormResponseSchema.omit({ updatedAt: true, createdAt: true  });
const CreateFamily = CreateFamilyFormSchema.omit({ updatedAt: true }); 
const UpdateFamily = UpdateFamilyFormSchema.omit({ updatedAt: true}); 

export async function createInvoice(formData: FormData) {
  const { customerId, amount, status } = CreateInvoice.parse({
    customerId: formData.get('customerId'),
    amount: formData.get('amount'),
    status: formData.get('status'),
  });
  const amountInCents = amount * 100;
  const date = new Date().toISOString().split('T')[0];
 
  await sql`
    INSERT INTO invoices (customer_id, amount, status, date)
    VALUES (${customerId}, ${amountInCents}, ${status}, ${date})
  `;
 
  revalidatePath('/dashboard/invoices');
  redirect('/dashboard/invoices');

}

export async function createFamily(formData: FormData) {
  const { name, email, members } = CreateFamily.parse({
    name: formData.get('name'),
    email: formData.get('email'),
    members: formData.get('members'),
  });
  await sql`
    INSERT INTO families (name, email, members)
    VALUES (${name}, ${email}, ${members})
  `;
  revalidatePath('/home/families');
  redirect('/home/families');
}

export async function updateFamily(formData: FormData) {
  console.log('updateFamily formData', formData)
  const { id, name, email, members }: any= UpdateFamily.parse({
    id: formData.get('id'),
    name: formData.get('name'),
    email: formData.get('email'),
    members: formData.get('members'),
  });

  console.log('updateFamily formData desctutued', id, name, email, members)

  await sql`
    UPDATE  families set (name, email, members)
    = (${name}, ${email}, ${members})
    WHERE id = ${id}
  `;
  revalidatePath(`/home/families/${id}/edit`);
  revalidatePath(`/home/families/`);

  redirect('/home/families/');
}

export async function createResponse(formData: FormData) {
  console.log('form Data', formData)
  const { status, adultOneFirstname, adultOneLastname, adultTwoFirstname, adultTwoLastname, childOneFirstname, childOneLastname, childTwoFirstname, childTwoLastname } = CreateResponse.parse({
    status: formData.get('status'),
    adultOneFirstname: formData.get('adultOneFirstname'),
    adultOneLastname: formData.get('adultOneLastname'),
    adultTwoFirstname: formData.get('adultTwoFirstname') || '',
    adultTwoLastname: formData.get('adultTwoLastname') || '',

    childOneFirstname: formData.get('childOneFirstname') || '',
    childOneLastname: formData.get('childOneLastname') || '',
    childTwoFirstname: formData.get('childTwoFirstname') || '',
    childTwoLastname: formData.get('childTwoLastname') || '',
  });
  
  const date = new Date().toISOString().split('T')[0];

  let total_adults = status === 'oui' ? 1 : 0;
  let total_children = 0;

  if(adultTwoFirstname && adultTwoLastname) {

    total_adults =+ 1;
  } 

  if(childOneFirstname || childOneLastname) {

    total_children =+ 1;
  } 

  if(childTwoFirstname || childTwoLastname) {

    total_children =+ 1;
  } 


  await sql`
    INSERT INTO guests ( status, adult_one_firstname, adult_one_lastname, adult_two_firstname, adult_two_lastname, child_one_firstname, child_one_lastname, child_two_firstname, child_two_lastname,  updated_at, created_at, total_adults, total_children)
    VALUES ( ${status}, ${adultOneFirstname}, ${adultOneLastname} , ${adultTwoFirstname} , ${adultTwoLastname}, ${childOneFirstname}, ${childOneLastname} , ${childTwoFirstname}, ${childTwoLastname}, ${date}, ${date}, ${total_adults}, ${total_children})
  `;
 
  revalidatePath('/thankyou');
  redirect('/thankyou');

}

export async function deleteGuest(id: string) {
  noStore();

  try {
    const guest = await sql`DELETE from guests where id=${id}`;
    //return guest.rows[0] as GuestsTable;
  } catch (error) {
    console.error('Failed to delete a GUETS:', error);
    throw new Error('Failed to fetch user.');
  }
  revalidatePath('/home/guests');

}

export async function deleteFamily(id: string) {
  noStore();

  try {
    const guest = await sql`DELETE from families where id=${id}`;
    //return guest.rows[0] as GuestsTable;
  } catch (error) {
    console.error('Failed to delete a families:', error);
    throw new Error('Failed to delete families.');
  }
  revalidatePath('/home/families');
}



export async function authenticate(
  prevState: string | undefined,
  formData: FormData,
) {
  try {
    console.log('authenticating...')

    const user = await signIn('credentials', formData);

    revalidatePath('/home/private/guests');
    redirect('/home/private/guests');


  } catch (error) {
    if (error instanceof AuthError) {
      switch (error.type) {
        case 'CredentialsSignin':
          return 'Invalid credentials.';
        default:
          return 'Something went wrong.';
      }
    }
    throw error;
  }
}