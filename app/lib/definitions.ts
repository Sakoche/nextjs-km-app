// This file contains type definitions for your data.
// It describes the shape of the data, and what data type each property should accept.
// For simplicity of teaching, we're manually defining these types.
// However, these types are generated automatically if you're using an ORM such as Prisma.
export type User = {
  id: string;
  name: string;
  email: string;
  password: string;
};

export type Customer = {
  id: string;
  name: string;
  email: string;
  image_url: string;
};

export type Invoice = {
  id: string;
  customer_id: string;
  amount: number;
  date: string;
  // In TypeScript, this is called a string union type.
  // It means that the "status" property can only be one of the two strings: 'pending' or 'paid'.
  status: 'pending' | 'paid';
};

export type Revenue = {
  month: string;
  revenue: number;
};

export type LatestInvoice = {
  id: string;
  name: string;
  image_url: string;
  email: string;
  amount: string;
};

// The database returns a number for amount, but we later format it to a string with the formatCurrency function
export type LatestInvoiceRaw = Omit<LatestInvoice, 'amount'> & {
  amount: number;
};

export type InvoicesTable = {
  id: string;
  customer_id: string;
  name: string;
  email: string;
  image_url: string;
  date: string;
  amount: number;
  status: 'pending' | 'paid';
};

export type GuestsTable = {
  id: string;
  adult_one_firstname: string;
  adult_one_lastname: string;
  adult_two_firstname: string;
  adult_two_lastname: string;
  child_one_firstname: string;
  child_one_lastname: string;
  child_two_firstname: string;
  child_two_lastname: string;
  total_adults: string;
  total_children: string;
  updated_at: string;
  status: 'pending' | 'oui' | 'non';
};

export type CustomersTable = {
  id: string;
  name: string;
  email: string;
  image_url: string;
  total_invoices: number;
  total_pending: number;
  total_paid: number;
};

export type FormattedCustomersTable = {
  id: string;
  name: string;
  email: string;
  image_url: string;
  total_invoices: number;
  total_pending: string;
  total_paid: string;
};

export type CustomerField = {
  id: string;
  familly_id: string;
  number_of_adult: number;
  number_of_children: number;
  updated_at: string;
  status: 'pending' | 'oui'| 'non';
  name?: string;
};

export type GuestField = {
  id: string;
  name: string;
};

export type FamillyField = {
  id: string;
  name: string;
  email: string;
  members?: number;
};

export type InvoiceForm = {
  id: string;
  customer_id: string;
  amount: number;
  status: 'pending' | 'paid';
};


export type GuestForm = {
  id: string;
  familly_id: string;
  number_of_adult: string;
  number_of_children: number;
  updated_at: string;
  status: 'pending' | 'oui' | 'non';
};


export type FamilyForm = {
  id: string;
  name: string;
  email: string;
  members: number;
};
