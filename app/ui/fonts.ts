import { Inter, Lusitana, Mrs_Saint_Delafield, Cormorant_Garamond, Dancing_Script, Great_Vibes} from 'next/font/google';
 
export const inter = Inter({ subsets: ['latin'] });
export const lusitana = Lusitana({ weight: '400', subsets: ['latin'] });
export const mrs_Saint_Delafield = Mrs_Saint_Delafield({ weight: '400', subsets: ['latin'] });
export const cormorant_Garamond = Cormorant_Garamond({ weight: '400', subsets: ['latin'] });
export const dancing_Script = Dancing_Script({ weight: '400', subsets: ['latin'] });
export const great_Vibes = Great_Vibes({ weight: '400', subsets: ['latin'] });