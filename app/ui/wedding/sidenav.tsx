import Link from 'next/link';
import NavLinks from '@/app/ui/wedding/nav-links';
import { PowerIcon } from '@heroicons/react/24/outline';
import Logo from '../wedding/logo';
import { signOut } from '@/auth';


export default function SideNav() {
  
  return (
    <div className="flex h-full flex-col px-3 py-4 md:px-2">
      <Link
      
        className="hidden md:block mb-2 h-20 items-end justify-start rounded-md bg-gray-50 p-4 md:h-40 box-content	relative"
        href="/"
      >
        <div className=" w-full md:w-40 text-slate-700	 ">
          <Logo />
        </div>
      </Link>
      <div className="flex grow flex-row justify-between space-x-2 md:flex-col md:space-x-0 md:space-y-2">
        <NavLinks />
        <div className="hidden h-auto w-full grow rounded-md bg-gray-50 md:block"></div>
        {/* <form
          action={async () => {
            'use server';
            await signOut();
          }}
        >   
            <button className="flex h-[48px] grow items-center justify-center gap-2 rounded-md bg-gray-50 p-3 text-sm font-medium hover:bg-sky-100 hover:text-blue-600 md:flex-none md:justify-start md:p-2 md:px-3">
            <PowerIcon className="w-6" />
            <div className="hidden md:block">Sign Out</div> 
          </button>
        </form> */}
        {/* <Link
            href="/login"
            className="flex h-[48px] grow items-center justify-center gap-2 rounded-md bg-gray-50 p-3 text-sm font-medium hover:bg-sky-100 hover:text-blue-600 md:flex-none md:justify-start md:p-2 md:px-3">
              Se connecter
            </Link> */}
      </div>
    </div>
  );
}

