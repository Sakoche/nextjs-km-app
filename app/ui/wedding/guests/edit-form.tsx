'use client';

import { CustomerField, FamillyField, GuestForm } from '@/app/lib/definitions';
import {
  CheckIcon,
  ClockIcon,
  XMarkIcon,
  CurrencyDollarIcon,
  UserCircleIcon,
} from '@heroicons/react/24/outline';
import Link from 'next/link';
import { Button } from '@/app/ui/button';
import { fetchGuestById } from '@/app/lib/data';
import { cormorant_Garamond, mrs_Saint_Delafield } from '@/app/ui/fonts';
import { useState } from 'react';


export default function EditGuestForm({
  guest,
  families,
}: {
  guest: GuestForm;
  families: FamillyField[];
}) {
  const [status, setStatus] = useState<boolean | null>(null);

  console.log('guest', guest);
  return (
    <form>
      <div className="rounded-md bg-gray-50 p-4 md:p-6">
        {/* Customer Name */}
        <div className="mb-4">
          <label htmlFor="customer" className="mb-2 block text-sm font-medium">
            Choose customer
          </label>
          <div className="relative">
            <select
              id="customer"
              name="customerId"
              className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              defaultValue={guest.familly_id}
            >
              <option value="" disabled>
                Select a customer
              </option>
              {families.map((familly) => (
                <option key={familly.id} value={familly.id}>
                  {familly.name}
                </option>
              ))}
            </select>
            <UserCircleIcon className="pointer-events-none absolute left-3 top-1/2 h-[18px] w-[18px] -translate-y-1/2 text-gray-500" />
          </div>
        </div>

        {/* Invoice Status */}
        <fieldset>
          <legend className="mb-2 block text-sm font-medium">
            Set the invoice status
          </legend>
          <div className="rounded-md border border-gray-200 bg-white px-[14px] py-3">
            <div className="flex gap-4">
              <div className="flex items-center">
                <input
                  onClick={() => setStatus(true)}
                  id="pending"
                  name="status"
                  type="radio"
                  value="pending"
                  defaultChecked={guest?.status === 'pending'}
                  className="h-4 w-4 border-gray-300 bg-gray-100 text-gray-600 focus:ring-2 focus:ring-gray-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-gray-600"
                />
                <label
                  htmlFor="pending"
                  className="ml-2 flex items-center gap-1.5 rounded-full bg-gray-100 px-3 py-1.5 text-xs font-medium text-gray-600 dark:text-gray-300"
                >
                  Pending <ClockIcon className="h-4 w-4" />
                </label>
              </div>
              <div className="flex items-center">
                <input
                  onClick={() => setStatus(true)}
                  id="oui"
                  name="status"
                  type="radio"
                  value="oui"
                  checked={guest?.status === 'oui'}
                  defaultChecked={guest?.status === 'oui'}
                  className="h-4 w-4 border-gray-300 bg-gray-100 text-gray-600 focus:ring-2 focus:ring-gray-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-gray-600"
                />
                <label
                  htmlFor="paid"
                  className="ml-2 flex items-center gap-1.5 rounded-full bg-green-500 px-3 py-1.5 text-xs font-medium text-white dark:text-gray-300"
                >
                  Oui <CheckIcon className="h-4 w-4" />
                </label>
              </div>
              <div className="flex items-center">
                <input
                  onClick={() => setStatus(false)}
                  id="non"
                  name="status"
                  type="radio"
                  value="non"
                  checked={guest?.status === 'non'}
                  defaultChecked={guest?.status === 'pending'}
                  className="h-4 w-4 border-gray-300 bg-gray-100 text-gray-600 focus:ring-2 focus:ring-gray-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-gray-600"
                />
                <label
                  htmlFor="non"
                  className="ml-2 flex items-center gap-1.5 rounded-full bg-gray-100 px-3 py-1.5 text-xs font-medium text-gray-600 dark:text-gray-300"
                >
                  Non <XMarkIcon className="h-4 w-4" />
                </label>
              </div>
            </div>
          </div>
        </fieldset>
                 {/* Nombre de personnes */}
          { status && <fieldset>
          <div className="mb-4">
          <label htmlFor="amount" className="mb-2 block text-sm font-medium">
            Combien serez-vous ?
          </label>
          <div className="relative mt-2 rounded-md">
            <div className="relative py-3">
              <input
                required
                id="amount"
                name="numberOfAdult"
                max={4}
                type="number"
                step="1"
                placeholder="Nombre d'adultes"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
            </div>
            <div className="relative">
              <input
                required
                id="amount"
                name="numberOfChildren"
                max={2}
                type="number"
                step="1"
                placeholder="Nombre d'enfant (max 2)"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
            </div>
          </div>
          </div>
          </fieldset>}
          
      </div>
      <div className="mt-6 flex justify-end gap-4">
        <Link
          href="/home/guests"
          className="flex h-10 items-center rounded-lg bg-gray-100 px-4 text-sm font-medium text-gray-600 transition-colors hover:bg-gray-200"
        >
          Cancel
        </Link>
        <Button type="submit">Edit Invoice</Button>
      </div>
    </form>
  );
}
