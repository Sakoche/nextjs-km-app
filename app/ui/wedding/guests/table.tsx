import Image from 'next/image';
import InvoiceStatus from '@/app/ui/invoices/status';
import { formatDateToLocal, formatCurrency } from '@/app/lib/utils';
import { fetchFilteredGuests } from '@/app/lib/data';
import { DeleteGuest, UpdateGuest } from './buttons';

export default async function GuestsTable({
  query,
  currentPage,
}: {
  query: string;
  currentPage: number;
}) {
  const guests = await fetchFilteredGuests(query, currentPage);

  return (
    <div className="mt-6 flow-root">
      <div className="inline-block min-w-full align-middle">
        <div className="rounded-lg bg-gray-50 p-2 md:pt-0">
        
          <table className=" min-w-full text-gray-900 md:table">
            <thead className="rounded-lg text-left text-sm font-normal">
              <tr>
                <th scope="col" className="px-4 py-5 font-medium sm:pl-6">
                  Nom
                </th>
                <th scope="col" className="px-3 py-5 font-medium">
                  Adulte(s)
                </th>
                <th scope="col" className="px-3 py-5 font-medium">
                  Enfant(s)
                </th>
                <th scope="col" className="hidden md:block px-3 py-5 font-medium">
                  Date réponse
                </th>
                <th scope="col" className="px-3 py-5 font-medium">
                  Status
                </th>
                <th scope="col" className="hidden md:block relative py-3 pl-6 pr-3">
                  <span className="sr-only">Edit</span>
                </th>
              </tr>
            </thead>
            <tbody className="bg-white">
              {guests?.map((guest) => (
                <tr
                  key={guest.id}
                  className={`w-full border-b py-3 text-sm last-of-type:border-none [&:first-child>td:first-child]:rounded-tl-lg [&:first-child>td:last-child]:rounded-tr-lg [&:last-child>td:first-child]:rounded-bl-lg [&:last-child>td:last-child]:rounded-br-lg ${guest.status === 'oui' ? 'bg-green-50' : 'bg-red-50'}`}
                >
                  <td className="whitespace-nowrap py-3 pl-6 pr-3">
                    <div className="flex items-center gap-3">
                  
                      <p>{guest.adult_one_lastname} {guest.adult_one_firstname} </p>
                      
                    </div>
                  </td>
    
                  <td className="whitespace-nowrap px-3 py-3">
                    <div className="flex items-center justify-center gap-3">

                     <p> {guest.total_adults}</p>
                    </div>
                  </td>
                  <td className="whitespace-nowrap px-3 py-3">
                    {guest.total_children}
                  </td>
                  <td className="whitespace-nowrap px-3 py-3 hidden md:block ">
                    {formatDateToLocal(guest.updated_at)}
                  </td>
                  <td className="whitespace-nowrap px-3 py-3">
                    {guest.status === 'oui' ? 'LA': 'PAS LA'}
                  </td>
                  <td className="whitespace-nowrap py-3 pl-6 pr-3 hidden md:block ">
                    <div className="flex justify-end gap-3">
                      <DeleteGuest id={guest.id} />
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
