
import { cormorant_Garamond, mrs_Saint_Delafield, great_Vibes} from '@/app/ui/fonts';
import styles from './contact.module.css';

import clsx from 'clsx';
import Link from 'next/link';
import { LinkIcon, ArrowLeftCircleIcon } from '@heroicons/react/24/outline';
export  function Contact({
  searchParams,
}: {
  searchParams?: {
    query?: string;
    page?: string;
  };
}) {

  const query = searchParams?.query || '';
  // const currentPage = Number(searchParams?.page) || 1;  
  // const totalPages = await fetchInvoicesPages(query);
  
  return (
    <div className="min-h-screen  flex flex-1  flex-col w-full items-center pt-20 bg-[url('/wedding/home_images/bg.png')] bg-contain bg-no-repeat bg-center">
        <div className={` ${styles.appear1} pt-30  flex flex-col justify-center align-center  text-zinc-500 ${styles.invitSentence} relative text-lg  ${great_Vibes.className}`}>
          <h3 className=' pb-10'>Contact</h3>
          <hr className=" pt-10   text-zinc-800" />
          <p className={cormorant_Garamond.className}>DEBICHE Karim:  06 29 21 48 68</p>
        </div>

        
    </div>
  );
}