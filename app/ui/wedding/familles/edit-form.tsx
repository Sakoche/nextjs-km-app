'use client';

import { FamillyField} from '@/app/lib/definitions';
import {
 
  UserCircleIcon,
  UsersIcon,
} from '@heroicons/react/24/outline';
import Link from 'next/link';
import { Button } from '@/app/ui/button';
import { fetchFamillyById, fetchGuestById } from '@/app/lib/data';
import { useState } from 'react';
import { updateFamily } from '@/app/lib/actions';
import { useParams, useRouter } from 'next/navigation';


export default function  EditFamilyForm({
  familly,
}: {
  familly: any ;
}) {
  const params = useParams<{ id: string }>();
  const [name, setName] = useState(familly.name);
  const [email, setEmail] = useState(familly.email);

  const [members, setMembers] = useState(familly.members);
  

  return (
    familly && params?.id &&   <form action={updateFamily}>
      <div className="rounded-md bg-gray-50 p-4 md:p-6">
        <input name='id' value={params?.id} hidden/>
        {/* Customer Name */}
        <div className="mb-4">
          <label htmlFor="customer" className="mb-2 block text-sm font-medium">
            Nom de famille
          </label>
          <div className="relative">
          <input
                onChange={(e) => setName(e.currentTarget.value)}
                value={name}
                id="name"
                name="name"
                placeholder="Nom de famille"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
            <UserCircleIcon className="pointer-events-none absolute left-3 top-1/2 h-[18px] w-[18px] -translate-y-1/2 text-gray-500" />
          </div>
        </div>
        <div className="relative mt-2 rounded-md">
        <label htmlFor="customer" className="mb-2 block text-sm font-medium">
            Nombre de personnes
          </label>
            <div className="relative">
              <input
                onChange={(e) => setMembers(e.currentTarget.value)}
                value={members}
                id="members"
                type='number'
                name="members"
                placeholder="Nombre de personne"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
              <UsersIcon className="pointer-events-none absolute left-3 top-1/2 h-[18px] w-[18px] -translate-y-1/2 text-gray-500 peer-focus:text-gray-900" />
            </div>
          </div>
          <div className="relative mt-2 rounded-md">
        <label htmlFor="customer" className="mb-2 block text-sm font-medium">
            Email
          </label>
            <div className="relative">
              <input
                onChange={(e) => setEmail(e.currentTarget.value)}

                value={email}
                id="email"
                type='email'
                name="email"
                placeholder="Votre email"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
              <UsersIcon className="pointer-events-none absolute left-3 top-1/2 h-[18px] w-[18px] -translate-y-1/2 text-gray-500 peer-focus:text-gray-900" />
            </div>
          </div>
      </div>
      <div className="mt-6 flex justify-end gap-4">
        <Link
          href="/home/guests"
          className="flex h-10 items-center rounded-lg bg-gray-100 px-4 text-sm font-medium text-gray-600 transition-colors hover:bg-gray-200"
        >
          Cancel
        </Link>
        <Button type="submit">Modifier</Button>
      </div>
    </form>

  );
}
