import Image from 'next/image';
import InvoiceStatus from '@/app/ui/invoices/status';
import { formatDateToLocal, formatCurrency } from '@/app/lib/utils';
import { fetchFilteredFamilies, fetchFilteredGuests } from '@/app/lib/data';
import { DeleteFamily, UpdateFamily } from './buttons';

export default async function FamiliesTable({
  query,
  currentPage,
}: {
  query: string;
  currentPage: number;
}) {
  const families = await fetchFilteredFamilies(query, currentPage);

  return (
    <div className="mt-6 flow-root">
      <div className="inline-block min-w-full align-middle">
        <div className="rounded-lg bg-gray-50 p-2 md:pt-0">
          <div className="md:hidden">
            {families?.map((family: any) => (
              <div
                key={family.id}
                className="mb-2 w-full rounded-md bg-white p-4"
              >
                <div className="flex items-center justify-between border-b pb-4">
                  <div>
                    <div className="mb-2 flex items-center">
                      <Image
                        src={""}
                        className="mr-2 rounded-full"
                        width={28}
                        height={28}
                        alt={`${family.name}'s profile picture`}
                      />
                      <p>{family.name}</p>
                    </div>
                    <p className="text-sm text-gray-500">{family.email}</p>
                  </div>
                </div>
                <div className="flex w-full items-center justify-between pt-4">
                  <div>
                    {/* <p className="text-xl font-medium">
                      {formatCurrency(33)}
                    </p> */}
                    {/* <p>{formatDateToLocal(guest.updated_at)}</p> */}
                  </div>
                  <div className="flex justify-end gap-2">
                    <UpdateFamily id={family.id} />
                    <DeleteFamily id={family.id} />
                  </div>
                </div>
              </div>
            ))}
          </div>
          <table className="hidden min-w-full text-gray-900 md:table">
            <thead className="rounded-lg text-left text-sm font-normal">
              <tr>
                <th scope="col" className="px-4 py-5 font-medium sm:pl-6">
                  Name
                </th>
                <th scope="col" className="px-3 py-5 font-medium">
                  Email
                </th>
                <th scope="col" className="px-3 py-5 font-medium">
                  Nombre de personnes
                </th>
              
                <th scope="col" className="px-3 py-5 font-medium">
                  Status
                </th>
                <th scope="col" className="relative py-3 pl-6 pr-3">
                  <span className="sr-only">Edit</span>
                </th>
              </tr>
            </thead>
            <tbody className="bg-white">
              {families?.map((family: any) => (
                <tr
                  key={family.id}
                  className="w-full border-b py-3 text-sm last-of-type:border-none [&:first-child>td:first-child]:rounded-tl-lg [&:first-child>td:last-child]:rounded-tr-lg [&:last-child>td:first-child]:rounded-bl-lg [&:last-child>td:last-child]:rounded-br-lg"
                >
                  <td className="whitespace-nowrap py-3 pl-6 pr-3">
                    <div className="flex items-center gap-3">
                      {/* <Image
                        src={guest?.image_url}
                        className="rounded-full"
                        width={28}
                        height={28}
                        alt={`${guest.name}'s profile picture`}
                      /> */}
                      <p>{family.name}</p>
                    </div>
                  </td>
                  <td className="whitespace-nowrap px-3 py-3">
                    {family.email}
                  </td>
                  <td className="whitespace-nowrap px-3 py-3">
                    {family.members}
                  </td>
                 
                 
                
                  <td className="whitespace-nowrap py-3 pl-6 pr-3">
                    <div className="flex justify-end gap-3">
                      <UpdateFamily id={family.id} />
                      <DeleteFamily id={family.id} />
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
