import Image from "next/image";
import Link from "next/link";
import classes from "./rsvp.module.css";
import { cormorant_Garamond, mrs_Saint_Delafield } from '@/app/ui/fonts';

export const Rsvp = () => {
    return ( <div className={`flex flex-col  relative top-20 bg-zinc-50 text-zinc-800 ${classes.appear4} `}>
        <Link href="/home/rsvp" className={`${classes.container} `}>
            Merci de confirmer votre présence ici 
        </Link>
    </div>)
}