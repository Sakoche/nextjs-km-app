import { GlobeAltIcon } from '@heroicons/react/24/outline';
import { lusitana } from '@/app/ui/fonts';
import Image from 'next/image';

export default function AcmeLogo() {
  return (
    <div className={`w-full  h-[160px] `} >
      <Image
          className=''
            src="/wedding/home_images/debiche.png"
            fill={true}
            alt="hearts"
          />
    </div>
  );
}
