"use client";

import {
    UserGroupIcon,
    HomeIcon,
    QuestionMarkCircleIcon,
    PhoneIcon,
  } from '@heroicons/react/24/outline';
import { useSession } from 'next-auth/react';

  export const useLinks = () => {
    const session = useSession();
    console.log('session in links', session);
    
    const links =  [
      { name: 'Home', href: '/home', icon: HomeIcon },
      {
        name: 'Ma réponse',
        href: '/home/rsvp',
        icon: QuestionMarkCircleIcon,
      },
      { name: 'Contact', href: '/contact', icon: PhoneIcon },



    ];
    console.log('session ddf', session);

    if(session.data?.user){
        links.push({ name: 'Les invités', href: '/home/guests', icon: UserGroupIcon });
        links.push({ name: 'Les familles', href: '/home/families', icon: UserGroupIcon });
    }

    return {links};
  }