'use client';

import { CustomerField } from '@/app/lib/definitions';
import Link from 'next/link';
import {
  CheckIcon,
  ClockIcon,
  CurrencyDollarIcon,
  UserCircleIcon,
  XMarkIcon
} from '@heroicons/react/24/outline';
import { Button } from '@/app/ui/button';
import { createResponse } from '@/app/lib/actions';
import { useEffect, useState } from 'react';
import { cormorant_Garamond, mrs_Saint_Delafield, great_Vibes } from '@/app/ui/fonts';


export default function Form({ families }: { families: CustomerField[] }) {

  const [status, setStatus] = useState<boolean | null>(null);
  const [formDatatotalAdults, setTotalAdults] = useState<number>(0);
  const [totalChildren, setTotalChildren] = useState<number>(0);


  const handleSubmit = (formData: FormData) => {  
    const all = formData.values();
    const allValues = Array.from(all);
    console.log('allValues', allValues);
   createResponse(formData);
  }

  return (
    
    <form action={handleSubmit}>
      <div className="rounded-md bg-gray-50 p-4 md:p-6">
        {/* Customer Name */}
        <div className="mb-4 flex flex-col justify-center">
          <p className={`flex self-center text-3xl  text-zinc-500 pt-10 pb-5 ${great_Vibes.className}`}>
             Mariage de Myriam et Karim, 13 avril 2024
          </p>
          <hr className="w-1/2 self-center border-t-1 border-zinc-300" />
          <p className={` bg-zinc-50 text-zinc-600 pt-5  text-center font-extrabold text-3xl md:text-4xl ${cormorant_Garamond.className} flex self-center`}>
             Serez vous des nôtres ?
          </p> 

        </div>

        {/* Invoice Status */}
        <fieldset className='py-3'>
          <legend className="mb-2 block  font-medium">
          <p className={` bg-zinc-50 text-zinc-600  ${cormorant_Garamond.className} `}>
             * Réponse souhaitée avant le 15 mars svp
          </p> 
          </legend>
          <div className="rounded-md border border-gray-200 bg-white px-[14px] py-3">
            <div className="flex gap-4">
              <div className="flex items-center">
                <input
                  onChange={() => setStatus(true)}
                  id="oui"
                  name="status"
                  type="radio"
                  value="oui"
                  className="h-4 w-4 border-gray-300 bg-gray-100 text-gray-600 focus:ring-2 focus:ring-gray-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-gray-600"
                />
                <label
                  htmlFor="oui"
                  className="ml-2 flex items-center gap-1.5 rounded-full bg-green-500 px-3 py-1.5 text-xs font-medium text-white dark:text-gray-300"
                >
                  Oui <CheckIcon className="h-4 w-4" />
                </label>
              </div>
              <div className="flex items-center">
                <input
                  onChange={() => setStatus(false)}
                  id="non"
                  name="status"
                  type="radio"
                  value="non"
                  className="h-4 w-4 border-gray-300 bg-gray-100 text-gray-600 focus:ring-2 focus:ring-gray-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-gray-600"
                />
                <label
                  htmlFor="non"
                  className="ml-2 flex items-center gap-1.5 rounded-full bg-red-500 px-3 py-1.5 text-xs font-medium text-white dark:text-gray-300"
                >
                  Non <XMarkIcon className="h-4 w-4" />
                </label>
              </div>
            </div>
          </div>
        </fieldset>
     
         {/* Nombre de personnes */}
         { status && <fieldset>

         <div className="mb-4">
          <label htmlFor="amount" className={`mb-2 block text-sm font-medium ${cormorant_Garamond.className}`}>
            Adultes :
          </label>
          <div className="relative mt-2 rounded-md">
          <div className="relative py-3 flex gap-5 ">
             <input
                required
                id="adultOneLastname"
                name="adultOneLastname"
                placeholder="Nom "
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
               <input
                required

                id="adultOneFirstname"
                name="adultOneFirstname"
               
                step="1"
                placeholder="Prénom"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
            </div>

            <div className="relative py-3 flex gap-5 ">
              <input

                id="adultTwoLastname"
                name="adultTwoLastname"
               
                step="1"
                placeholder="Nom"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
               <input
                id="adultTwoFirstname"
                name="adultTwoFirstname"
                placeholder="Prénom"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
            </div>
            
          </div>
       
       
       
          <label htmlFor="amount" className={`mb-2 block text-sm font-medium ${cormorant_Garamond.className}`}>
            Enfants :
          </label>
          <div className="relative mt-2 rounded-md">
          <div className="relative py-3 flex gap-5 ">
             <input
                id="childOneLastname"
                name="childOneLastname"
               
                step="1"
                placeholder="Nom "
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
               <input
                 
                id="childOneFirstname"
                name="childOneFirstname"
               
                step="1"
                placeholder="Prénom"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
            </div>

            <div className="relative py-3 flex gap-5 ">
              <input
                 
                id="childTwoLastname"
                name="childTwoLastname"
                placeholder="Nom"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
               <input
                 
                id="childTwoFirstname"
                name="childTwoFirstname"
                placeholder="Prénom"
                className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
              />
            </div>
            
          </div>
          <legend className={`mb-2 block text-sm font-medium text-gray-600 ${cormorant_Garamond.className}`}>
            * Pensez à réserver votre nounou &#128521; !
        </legend>
       
        </div>
       
        </fieldset>}

        { status === false && <fieldset>

      <div className="mb-4">
      <label htmlFor="amount" className={`mb-2 block text-sm font-medium ${cormorant_Garamond.className}`}>
      Votre nom:
      </label>
      <div className="relative mt-2 rounded-md">
      <div className="relative py-3 flex gap-5 ">
    <input
       required
       id="adultOneLastname"
       name="adultOneLastname"
       placeholder="Nom "
       className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
     />
      <input
       required

       id="adultOneFirstname"
       name="adultOneFirstname"
      
       step="1"
       placeholder="Prénom"
       className="peer block w-full rounded-md border border-gray-200 py-2 pl-10 text-sm outline-2 placeholder:text-gray-500"
     />
   </div>
   
 </div>






</div>

</fieldset>}
       
      </div>
      
      <div className="mt-6 flex justify-end gap-4">
        {/* <Link
          href="/dashboard/invoices"
          className="flex h-10 items-center rounded-lg bg-gray-100 px-4 text-sm font-medium text-gray-600 transition-colors hover:bg-gray-200"
        >
          Annuler
        </Link> */}
        <Button type="submit" className={status === null ? "bg-gray-100 hover:bg-gray-100 active:bg-gray-100" : ""} disabled={status===null}>Envoyer ma réponse</Button>
      </div>
    </form>
  );
}
