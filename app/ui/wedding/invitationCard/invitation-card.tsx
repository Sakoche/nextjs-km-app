import Image from "next/image";
import Link from "next/link";
import styles from "./invitation-card.module.css";
import { cormorant_Garamond, mrs_Saint_Delafield, dancing_Script, great_Vibes } from '@/app/ui/fonts';
import { Rsvp } from "../rsvp/rsvp";
import clsx from "clsx";

export const InvitationCard = () => {

   return ( <div className={clsx( styles.container,` flex flex-1 max-w-screen-lg  shadow-md rounded-lg items-center pt-36 pb-36 justify-center flex-col  w-2/5 relative bg-white w-full text-zinc-400 ${cormorant_Garamond.className}`)}>

    <div className={`flex items-center w-5/5 flex-col flex-1 	
     bg-[url('/wedding/home_images/bg.png')] bg-contain bg-no-repeat bg-center  ${styles.card} ${styles.bg} ${styles.zoomInOutBox} `}>
    <div className={` ${styles.appear1} mb-5 relative flex flex-col justify-center items-center  h-[188px] w-[295px] max-w-[295px]   ${styles.title} font-extrabold	 text-zinc-400		`}>
        <p>MYRIAM</p>
        <p>& KARIM</p>
     </div>
    <div className={` ${styles.appear3} p-0 flex flex-col text-xs/11  text-zinc-500 ${styles.invitSentence} relative  ${great_Vibes.className}`}>
       <p className="md:hidden">Les familles</p>
       <p className="md:hidden">  Debiche  et El Feddali</p>
       <p className="hidden md:block"> Les familles Debiche  et El Feddali</p>

       <p>ont le plaisir de vous inviter</p>
       <p>au mariage de leurs  enfants,</p>
       <p>Myriam et Karim.</p>

    </div>
    
    <div className={`${styles.appear4}  mb-4  text-zinc-600 relative   ${styles.date}`}>
        Samedi, 13 avril, 2024
    </div>
       
    <div className={` ${styles.appear4}  mb-4  text-zinc-700 flex flex-col items-center justify-center p-1  text-center ${styles.adress}`}>
                <p className={`flex self-center p-5`}>Rendez-vous à la mairie de Soisy-sous-Montmorency</p>
                <p>2 avenue du Général De Gaulle, 95230</p>

                <p>à 14 heures.</p>
                <p>Puis à 18h30 à la salle de récèption l&apos; Honoré,</p>
                <p>12 rue de Paris, Domont 95330</p>
                <Rsvp />
    </div>

    </div>
    
    


    </div>)
}